**More information at [www.mackelab.org](www.mackelab.org)**

info@mackelab.org

This listing also includes packages for which a substantial portion of the code was developed by collaborators of the group, please see details and references in individual repositories.

## pop_spike: [bitbucket.org/mackelab/pop_spike](bitbucket.org/mackelab/pop_spike)

Dichotomized Gaussian and maximum entropy models for correlated spiking, binary spiking or discrete spike counts

## pop_spike_dyn: [bitbucket.org/mackelab/pop_spike_dyn](bitbucket.org/mackelab/pop_spike_dyn)

Methods for fitting dynamical system models with Poisson observations. Much of this code was written by [Lars Buesing](http://www.gatsby.ucl.ac.uk/~lars/).

## vits_hmm: [bitbucket.org/mackelab/vits_hmm](bitbucket.org/mackelab/vits_hmm)

Methods for fitting tree-structured hidden markov models with binary observations, based on the [NIPS paper](http://www.kyb.tuebingen.mpg.de/fileadmin/user_upload/files/publications/2014/NIPS-2014-Putzky-Paper.pdf)  by Patrick Putzky, Florian Franzen, Giacomo Bassetto and Jakob Macke, [supplement](http://www.kyb.tuebingen.mpg.de/fileadmin/user_upload/files/publications/2014/NIPS-2014-Putzky-Suppl.pdf)

## serial_decision: [bitbucket.org/mackelab/serial_decision](bitbucket.org/mackelab/serial_decision)

Modelling inter-trial dependence  in  psychophysics (Ingo Fründ and  Felix Wichmann)

## gp_maps: [bitbucket.org/mackelab/gp_maps](bitbucket.org/mackelab/gp_maps)      

Gaussian process methods for modelling cortical maps

## psignifit4:  [github.com/wichmann-lab/psignifit](github.com/wichmann-lab/psignifit)

 Bayesian Psychometric function fitting without a   pain in the neck  (Heiko Schütt and Stefan Harmeling and Felix Wichmann)